CREATE TABLE "USER" (
  "id" VARCHAR(42),
  "email" VARCHAR(42),
  "password" VARCHAR(42),
  PRIMARY KEY ("id")
);

CREATE TABLE "DO" (
  "id" VARCHAR(42),
  "idlab" VARCHAR(42),
  PRIMARY KEY ("id", "idlab")
);

CREATE TABLE "LAB" (
  "idlab" VARCHAR(42),
  "title" VARCHAR(42),
  PRIMARY KEY ("idlab")
);

CREATE TABLE "BELONG" (
  "idgroup" VARCHAR(42),
  "id" VARCHAR(42),
  PRIMARY KEY ("idgroup", "id")
);

CREATE TABLE "GROUP" (
  "idgroup" VARCHAR(42),
  "name" VARCHAR(42),
  PRIMARY KEY ("idgroup")
);

CREATE TABLE "DATA" (
  "iddata" VARCHAR(42),
  "label" VARCHAR(42),
  "values" VARCHAR(42),
  "idlab" VARCHAR(42),
  PRIMARY KEY ("iddata")
);

ALTER TABLE "DO" ADD FOREIGN KEY ("idlab") REFERENCES "LAB" ("idlab");
ALTER TABLE "DO" ADD FOREIGN KEY ("id") REFERENCES "USER" ("id");
ALTER TABLE "BELONG" ADD FOREIGN KEY ("id") REFERENCES "USER" ("id");
ALTER TABLE "BELONG" ADD FOREIGN KEY ("idgroup") REFERENCES "GROUP" ("idgroup");
ALTER TABLE "DATA" ADD FOREIGN KEY ("idlab") REFERENCES "LAB" ("idlab");