// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
// var browserify = require('browserify');
// var mysql = require('mysql');
//
// var connection = mysql.createConnection({
//   host: "localhost",
//   user: "sammy",
//   password: "password",
//   database: "metrologia"
// });
// connection.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
// })
