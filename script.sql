DROP TABLE if exists USER;
DROP TABLE if exists ROLE;

create table ROLE(
  IdRole int(15),
  Label varchar(20),
  PRIMARY KEY (IdRole)
);
create table USER(
  IdUser int(15),
  Username varchar(20),
  Surname varchar(20),
  Password varchar(20),
  Role int(15),
  PRIMARY KEY (IdUser),
  FOREIGN KEY (Role) references ROLE (IdRole)
);
insert into ROLE values
  (0,'teacher'),
  (1,'student');
 insert into USER values
   (1, 'Paul', 'Smith','a', 0),
   (2, 'Pauul', 'azer','a', 1);
